Notes:
- the project don't implements autoloaders, front-controller, bootstrap, routers, .env etc.
- uses docker for the database, use root:demopass or set a user in the docker-compose.yml. If you don't want to use docker please configure your own mariadb/mysql server/port
- don't use the default root:demopass in production or live env
- the validation rules are stored in DB named config

1. Run the docker from folder by using:
docker-compose up -d
the docker configuration use port 3708

2. Create the tables
-- start copy from here
-- table game
CREATE TABLE game (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255) UNIQUE,
    state INT,
    picture VARCHAR(255),
    create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- table config
CREATE TABLE config (
    id INT AUTO_INCREMENT PRIMARY KEY,
    max_file_size INT,
    allowed_extensions VARCHAR(255),
    allowed_mime_types VARCHAR(255)
);

INSERT INTO config (max_file_size, allowed_extensions, allowed_mime_types)
VALUES (5242880, 'jpg,png,jpeg', 'image/jpeg,image/png');

-- table state
CREATE TABLE state (
    id INT AUTO_INCREMENT PRIMARY KEY,
    name VARCHAR(255)
);

INSERT INTO state (name) VALUES ('State 1');
INSERT INTO state (name) VALUES ('State 2');
INSERT INTO state (name) VALUES ('State 3');
INSERT INTO state (name) VALUES ('State 4');
INSERT INTO state (name) VALUES ('State 5');

ALTER TABLE game
ADD FOREIGN KEY (state) REFERENCES state(id);
-- ends here