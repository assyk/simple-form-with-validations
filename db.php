<?php

/**
 * Class Database
 */
class Database {
    private static $instance = null;
    private $connection;

    private $host = '127.0.0.1';
    private $dbname = 'oengine';
    private $user = 'root';
    private $password = 'demopass';
    private $port = 3708;

    private function __construct() {
        try {
            $this->connection = new PDO("mysql:host=$this->host;dbname=$this->dbname;port=$this->port", $this->user, $this->password);
            $this->connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
            exit;
        }
    }

    public static function getInstance() {
        if (self::$instance === null) {
            self::$instance = new Database();
        }
        return self::$instance;
    }

    public function getConnection() {
        return $this->connection;
    }
}