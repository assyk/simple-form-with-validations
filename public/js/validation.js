$(document).ready(function() {
    // Add an event listener to the form's submit event
    $('#game-form').submit(function(event) {
        // Prevent the default form submission behavior
        event.preventDefault();

        // Get the form data
        var formData = new FormData(this);

        // Submit the form data via AJAX
        $.ajax({
            url: 'router.php',
            type: 'POST',
            data: formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success: function(response) {
                // Handle the response from the server
                if (response.errors) {
                    // If there are errors, display them in the form
                    displayErrors(response.errors);
                } else {
                    // If there are no errors, redirect to the index page
                    window.location.href = 'index.php?last_id=' + response.success;
                }
            }
        });
    });
});

function displayErrors(errors) {
    // Clear any existing error messages
    $('.is-invalid').removeClass('is-invalid');
    $('.invalid-feedback').empty();

    // Display the error messages in the form
    $.each(errors, function(name, message) {
        $('#' + name).addClass('is-invalid');
        $('#' + name).siblings('.invalid-feedback').text(message).show();
    });
}