<?php

/**
 * Class Config
 */
class Config
{
    private PDO $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    public function update($data)
    {

    }

    /**
     * @return array
     */
    public function getConfig(): array
    {
        $stmt = $this->db->query("SELECT * FROM config");
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
        
        if ($row) {
            $config = [
                'max_file_size' => $row['max_file_size'],
                'allowed_extensions' => explode(',', $row['allowed_extensions']),
                'allowed_mime_types' => explode(',', $row['allowed_mime_types'])
            ];
        } else {
            $config = [
                'max_file_size' => '',
                'allowed_extensions' => [],
                'allowed_mime_types' => []
            ];
        }

        return $config;
    }
}
