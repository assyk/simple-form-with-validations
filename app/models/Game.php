<?php

// Game.php

class Game
{
    private PDO $db;

    public function __construct(PDO $db)
    {
        $this->db = $db;
    }

    /**
     * @param $data
     * @return int last insert id
     */
    public function insert($data): int
    {

        $stmt = $this->db->prepare("INSERT INTO game (name, state, picture, create_time) VALUES (:name, :state, :picture, NOW())");
        $stmt->bindParam(':name', $data['name']);
        $stmt->bindParam(':state', $data['state']);
        $stmt->bindParam(':picture', $data['picture']);
        $stmt->execute();

        return $this->db->lastInsertId();
    }

    public function nameExists($name): bool
    {
        $name = htmlspecialchars(trim($name));
        $stmt = $this->db->prepare("SELECT COUNT(*) FROM game WHERE name = :name");
        $stmt->bindParam(':name', $name);
        $stmt->execute();

        return (bool) $stmt->fetchColumn();
    }
}
