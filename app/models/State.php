<?php


// State.php

class State
{
    public static function getStates(PDO $db)
    {
        $stmt = $db->query("SELECT * FROM state");
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }
}