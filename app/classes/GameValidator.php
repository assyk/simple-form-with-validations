<?php
require_once 'app/models/Config.php';

/**
 * Class GameValidator
 * validates the fields - name and picture
 */
class GameValidator
{
    private PDO $db;
    private Config $configModel;

    public function __construct(PDO $db)
    {
        $this->db = $db;
        $this->configModel = new Config($db);
        $this->gameModel = new Game($db);
    }

    /**
     * @param array $data form data and $_FILES should be available
     * @return array $errors or empty array
     */
    public function validate(array $data): array
    {
        $errors = [];

        $name = trim($data['name']);
        // Validate name
        if (empty(trim($data['name']))) {
            $errors['name'] = 'Name is required';
        } elseif (!preg_match('/^[a-zA-Z0-9\s]+$/', trim($data['name']))) {
            $errors['name'] = 'Name can only contain letters, numbers and spaces';
        } else {

            if ($this->gameModel->nameExists($name)) {
                $errors['name'] = 'Name is already taken';
            }
        }

        // Validate picture
        if (!isset($_FILES['picture'])) {
            $errors['picture'] = 'Picture is required';
        } else {
            // get stored validation params from config DB
            $config = $this->configModel->getConfig();

            $allowed_extensions = $config['allowed_extensions'];
            $allowed_mime_types = $config['allowed_mime_types'];
            $max_file_size = $config['max_file_size'];
            $filesize_in_mb = round($max_file_size / 1048576, 2);

            $file_type = pathinfo($_FILES['picture']['name'], PATHINFO_EXTENSION);
            $file_size = $_FILES['picture']['size'];
            $file_mime_type = mime_content_type($_FILES['picture']['tmp_name']);


            if ($_FILES['picture']['error'] === UPLOAD_ERR_INI_SIZE || $_FILES['picture']['error'] === UPLOAD_ERR_FORM_SIZE) {
                $errors['picture'] = 'File size exceeds maximum of '.$filesize_in_mb.' MB';
            } elseif (!in_array($file_type, $allowed_extensions)) {
                $errors['picture'] = 'Invalid file type. Allowed types: ' . implode(', ', $allowed_extensions);
            } elseif (!in_array($file_mime_type, $allowed_mime_types)) {
                $errors['picture'] = 'Invalid file format. Allowed formats: ' . implode(', ', $allowed_mime_types);
            } elseif ($file_size > $max_file_size) {
                $errors['picture'] = 'File size exceeds maximum of '.$filesize_in_mb.' MB';
            }
        }

        return $errors;
    }
}

