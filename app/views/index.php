<?php
?>
<!DOCTYPE html>
<html>
<head>
    <title>Game Form</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.6.0/css/bootstrap.min.css" />
</head>
<body>
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-6">

            <form id="game-form" method="post" action="/<?= $dirName ?>/router.php" enctype="multipart/form-data">
                <h3 class="mb-4">Game Form</h3>

                <div class="form-group">
                    <div style="background: red; color: white"><?php echo  $upload_dir_check ?? ''?></div>
                </div>

                <div class="form-group">
                    <label for="name">Name:*</label>
                    <input id="name" type="text" name="name" class="form-control" required>
                    <div class="invalid-feedback"></div>
                </div>

                <div class="form-group">
                    <label for="state">State:*</label>
                    <select id="state" name="state" class="form-control" required>
                        <?php foreach ($states as $state) : ?>
                            <option value="<?= $state['id'] ?>"><?= $state['name'] ?></option>
                        <?php endforeach ?>
                    </select>
                </div>

                <div class="form-group">
                    <label for="picture">Picture:*</label>
                    <input id="picture" type="file" name="picture" class="form-control-file" required>
                    <div class="invalid-feedback"></div>
                </div>
                <input type="hidden" name="game" value="game_form">
                <!-- CRLF-->
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
            <div><br></div>
            <div class="row">
                <div class="alert alert-error">
                    ** Check the server config: upload_max_filesize &  post_max_size. If they are under the min req size may produces an error.
                    <div id="misc_errors"></div>
                    <div class="invalid-feedback"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="<?= BASE_URL ?>/public/js/validation.js"></script>

</body>
</html>