<?php

/**
 * Class GameController
 */
class GameController
{
    private PDO $db;
    private Game $gameModel;

    public function __construct(PDO $db)
    {
        $this->db = $db;
        $this->gameModel = new Game($db);
    }

    /**
     * @param array $data
     * @return string with json response errors|success
     */
    public function submitForm(array $data): string
    {

        // Validate the form data
        $validator = new GameValidator($this->db);
        $errors = $validator->validate($data, $_FILES);

        // if no errors insert
        if (empty($errors)) {
            // Insert the form data into the database
            if (isset($_FILES['picture']['name'])) {
                $file_name  = $_FILES['picture']['name'] = str_replace(' ', '_', trim(basename($_FILES['picture']['name'])));
            }

            // Insert in DB
            $lastInsertId = $this->gameModel->insert([
                'name' => htmlspecialchars(trim($data['name'])),
                'state' => (int)$data['state'],
                'picture' => htmlspecialchars($file_name ?? ''),
            ]);

            if (isset($_FILES) && !empty($_FILES)) {
                $uploadDir = 'public/uploads/';
                $uploadFile = $uploadDir . $lastInsertId . '_' . $file_name;
                move_uploaded_file($_FILES['picture']['tmp_name'], $uploadFile);
            }

            return json_encode(['success' => $lastInsertId]);

        } else {

            return json_encode(['errors' => $errors]);

        }
    }
}
