<?php
error_reporting(E_ALL);
ini_set('display_errors', 0);

// needs autoloaders ...
require_once 'configs.php'; // important
require_once 'functions.php';
require_once 'db.php';
require_once 'app/models/State.php';

// prepare the database connection
$conn = Database::getInstance();
$db = $conn->getConnection();

// takes data from db for the dropdown
$states = State::getStates($db);

// Needs a front-controller, bootstrap, routers
require_once 'app/views/index.php';