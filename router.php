<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
//echo '<pre>22:>'; echo print_r($_SERVER['REQUEST_METHOD'],1); echo "</pre><br>";
//echo '<pre>3333:>'; echo print_r($_POST,1); echo "</pre><br>"; die();
/**
 * Used for temp/example routing
 */


// Initialize models and controllers
require_once 'db.php';
$conn = Database::getInstance();
$db = $conn->getConnection();

require_once __DIR__ . '/app/models/Game.php';
require_once __DIR__ . '/app/models/State.php';
require_once __DIR__ . '/app/classes/GameValidator.php';
require_once __DIR__ . '/app/controllers/GameController.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // checks for over sized file and 'Request has been truncated'
    if (empty($_POST)) {
        echo json_encode(['errors' => ['misc_errors' => 'Error: Check the file size of uploaded file']]);
        exit();
    }

    if (isset($_POST['game'])) { // comes from the game form
        $gameController = new GameController($db);
        $result = $gameController->submitForm($_POST, $_FILES);
        echo $result;
        exit();
    }
}