<?php
/**
 * Basic configs and Constants
 */

$url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
$dirName = basename(dirname(__FILE__));
$full_url = $url . '/' . $dirName;
define('BASE_URL', $full_url);

$uploadDir = __DIR__ . '/public/uploads/';
if (!is_writable($uploadDir)) {
    $upload_dir_check = $uploadDir . ' The uploads directory is not writable.';
}